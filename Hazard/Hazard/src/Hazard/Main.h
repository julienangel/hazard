#pragma once

#include "Core.h"

namespace Hazard
{
	class HAZARD_API Main
	{
	public:
		Main();
		virtual ~Main();

		void Run();
	};

	// To be defined in ClIENT
	Main* CreateApplication();
}


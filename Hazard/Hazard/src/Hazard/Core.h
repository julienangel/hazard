#pragma once

#ifdef HAZARD_PLATFORM_WINDOWS
	#ifdef HAZARD_BUILD_DLL
		#define HAZARD_API __declspec(dllexport)
	#else
		#define HAZARD_API __declspec(dllimport)
	#endif
#else
	#error Hazard only prepared for Windows!
#endif // 

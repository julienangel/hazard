#pragma once

#ifdef HAZARD_PLATFORM_WINDOWS

extern Hazard::Main* Hazard::CreateApplication();

int main(int argc, char** argv)
{
	printf("Hazard Engine\n");
	auto app = Hazard::CreateApplication();
	app->Run();
	delete app;
}

#endif
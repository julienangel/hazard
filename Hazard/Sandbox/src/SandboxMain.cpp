#include <Hazard.h>

class Sandbox : public Hazard::Main
{
public:
	Sandbox()
	{

	}

	~Sandbox()
	{
		
	}
};

Hazard::Main* Hazard::CreateApplication()
{
	return new Sandbox();
}